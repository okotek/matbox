package matbox

import (
	"strconv"
	"time"

	"gitlab.com/ashinnv/oddstring"
	"gitlab.com/okotek/okoframe"
	"gocv.io/x/gocv"
)

type MatBox struct {
	Mat       gocv.Mat
	TimeStamp time.Time
	CamId     string
	Owner     string
}

func (mb MatBox) ConsumeMat(inputMat gocv.Mat, camId, owner string) MatBox {

	return MatBox{
		inputMat,
		time.Now(),
		camId,
		owner,
	}
}

func (mb MatBox) ConvertToFrame() okoframe.Frame {
	retFrm := okoframe.Frame{
		oddstring.RandStringSingle(127) + strconv.Itoa(int(mb.TimeStamp.UnixNano())),
		uint64(mb.TimeStamp.UnixNano()),
		uint64(mb.Mat.Cols()),
		uint64(mb.Mat.Rows()),
		uint8(mb.Mat.Channels()),
		mb.Mat.Type(),
		mb.Mat.ToBytes(),

		mb.Owner,
		mb.CamId,
		"0.1",

		[]okoframe.Detection{},
	}

	return retFrm
}

func MatBoxChanFactory(matChan chan gocv.Mat, output chan MatBox, camId, owner string) {
	for input := range matChan {

		tmpMat := MatBox{
			input,
			time.Now(),
			camId,
			owner,
		}

		output <- tmpMat
	}
}
