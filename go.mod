module matbox

go 1.22.0

require (
	gitlab.com/ashinnv/oddstring v0.1.1
	gitlab.com/okotek/okoframe v0.0.1
	gocv.io/x/gocv v0.35.0
)

replace gitlab.com/okotek/okoframe => ../okoframe
